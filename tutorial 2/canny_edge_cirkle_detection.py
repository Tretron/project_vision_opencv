# -*- coding: utf-8 -*-
"""
Created on Tue Aug 29 15:50:52 2017

@author: tretron
"""

import cv2
import time
import numpy as np
import math

low_threshold=70

high_threshold=125

#cirkletransformatie
dp = 2
minDist = 30
param1 = 480/8
param2= 80
minRadius = 10
maxRadius = 30


cv2.namedWindow("Grey_image",cv2.WINDOW_AUTOSIZE)
cv2.namedWindow("Canny_image",cv2.WINDOW_AUTOSIZE)

cv2.moveWindow("Grey_image", 0,0)
cv2.moveWindow("canny_image",600,0)

webcam = cv2.VideoCapture(0)

webcam.set(cv2.CAP_PROP_FRAME_WIDTH,640)
webcam.set(cv2.CAP_PROP_FRAME_HEIGHT,480)

webcam.get(cv2.CAP_PROP_FRAME_WIDTH)

while True:
    retval,img = webcam.read()
    if retval != True: break
    img_grey = cv2.cvtColor(img ,cv2.COLOR_BGR2GRAY)
    img_edges = cv2.Canny(img_grey,low_threshold,high_threshold)
    circles = cv2.HoughCircles(img_edges,cv2.HOUGH_GRADIENT, dp, minDist, None, param1, param2, minRadius, maxRadius)
    try:
        n = np.shape(circles)
        circles = np.reshape(circles,(n[1],n[2]))
        print(circles)
        for circle in circles:
            cv2.circle(img,(circle[0],circle[1]),circle[2],(0,0,255),2)
    except:
        print("no circles found")
    cv2.imshow("Grey_image", img)
    cv2.imshow("canny_image", img_edges)
    
    
    
    if cv2.waitKey(10) == 27: break

webcam.release()
cv2.destroyAllWindows()