# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

tutorial vision vor the hague university of applied sciences. the goal is to learn and understand opencv with python

### How do I get set up? ###

The project is set up in three main tutorials, each with their own assignments.
 -1 Tutorial 1, Inleiding Open CV
		Assignment 1: Write a programm that saves an image every half a second for ten seconds.
		The resulting images need to be converted to gray scale and avaraged out. The avarage displayed on the screen and saved as jpg file.
		File name should be gemiddelde.py with name, student number in the comments
		Assignment 2: Adjust the file warping.py zo the image is continuely rotated with a rotate frequency of 90 degrees/second
		Name the file rotatie.py and comment name student number.
	Tutorial 2, warming and feature extraction
		assingment 1: take the code and adjust it so every image is rotated 1 degree.
		test the code with resizing and varry the rotation
		assingment 2: Write a python programm that displays the image of the webcam and the image of canny edge.
		assingment 3: extent the canny edge detection code with cirkle detection in the webcam image
	Tutorial 3, socket comminucation.
		Write a server and client programm that sends a nump-array of 100 numbers to the server.
		The client add 1 to all numbers and sends them back to the server. the server then displays the number. 
		Use the python modules Struct en Socket.
	
	
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

made by laurens roos
to be checked by dhr. Kouwe