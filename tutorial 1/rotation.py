# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
# Name:        gemiddelde.py
# Purpose:     avarage out the grey scale of 20 images
#
# Author:      Laurens Roos
#
# Created:     28-8-2017
# Copyright:   (c) Laurens Roos 2017
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#warping
import cv2
import time
import numpy as np
import math

cv2.namedWindow("beeld1", cv2.WINDOW_AUTOSIZE)
cv2.namedWindow("beeld2", cv2.WINDOW_AUTOSIZE)
cv2.moveWindow("beeld1", 0,0)
cv2.moveWindow("beeld2",640,0)

webcam = cv2.VideoCapture(0)
retval,img = webcam.read()
height = img.shape[0]
width  = img.shape[1]

"""

rotation varriables

"""
rotation_rate = 2
time_interval = 0.01
start_time = time.time()
angle_deg = 0
new_time = start_time

scale = 1

x_center = width / 2
y_center = height / 2

while True:
    if ((new_time-time.time())<-time_interval):
        new_time = time.time()
        angle_deg+= rotation_rate
        angle_rad = (angle_deg/180.0) * math.pi
        alpha = scale * math.cos(angle_rad)
        beta = scale*math.sin(angle_rad)
    
        WarpMat = np.array([ [alpha, beta, (1- alpha)*x_center - beta * y_center], \
                              [-beta, alpha, beta * x_center +(1 - alpha )* y_center] ])
    # time.sleep(0.1)
    
    retval, img = webcam.read()
    if retval != True: break

    img_warp = cv2.warpAffine(img,WarpMat,(width,height))
    
    cv2.imshow("beeld1",img)
    cv2.imshow("beeld2",img_warp)
    
    if (cv2.waitKey(10) == 27):
        break

webcam.release()
cv2.destroyAllWindows()

