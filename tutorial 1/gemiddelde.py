import cv2               # importeer de opencv module in python
import time              # importeer de time module, nodig voor sleep
import numpy as np       # beelden zijn numpy arrays, numpy gebruiken we ook
n = 0

#-------------------------------------------------------------------------------
# Name:        gemiddelde.py
# Purpose:     avarage out the grey scale of 20 images
#
# Author:      Laurens Roos
#
# Created:     28-8-2017
# Copyright:   (c) Laurens Roos 2017
# Licence:     <your licence>
#-------------------------------------------------------------------------------

# maak een window aan met naam " beeld1 " om beelden weer te geven ,
# de afmetingen worden automatisch bepaald ( enig mogelijke optie )
# (de waarde van de optie cv2 .cv. CV_WINDOW_AUTOSIZE is 1, dit
# kun je evt . controleren in het Python Interpreter window )
cv2.namedWindow("beeld1",cv2.WINDOW_AUTOSIZE)
cv2.namedWindow("beeld2",cv2.WINDOW_AUTOSIZE)
cv2.namedWindow("Gemiddelde", cv2.WINDOW_AUTOSIZE)

# er kunnen uiteraard meerdere windows worden geopend , elk met hun eigen naam
# de naam dient er voor om de windows van elkaar te onderscheiden

# MoveWindow is een routine uit de 'oude' cv bibliotheek
# waarmee beeld1 wordt geplaatst in de linker bovenhoek
# van het scherm: pixel (0,0)
cv2.moveWindow("beeld1",0,0)
cv2.moveWindow("beeld2",300,0)
cv2.moveWindow("gemiddelde",600,0)

# open de 'default' video-beeld
# met VideoCapture(0), VideoCapture(1) 
# kunnen andere video beelds worden geopend
#webcam = cv2.VideoCapture(-1)
webcam = cv2.VideoCapture(0)

# met get kunnen deze properties opgevraagd worden , bijvoorbeeld :

# stel aantal pixels over de breedte in
webcam.set(cv2.CAP_PROP_FRAME_WIDTH,640)
# stel aantal pixels over de hoogte in
webcam.set(cv2.CAP_PROP_FRAME_HEIGHT,480) 
# stel de openingstijd in (in sec .) (dit werkt niet voor alle camera's)
# webcam.set(cv2.CAP_PROP_EXPOSURE,.2)      
webcam.get(cv2.CAP_PROP_FRAME_WIDTH)

img_grayold = np.zeros((480,640), dtype=np.uint16)
#img_grayav = np.zeros((480,640), dtype=np.uint8)
# voor meer opties zie :
# http://docs.opencv.org/modules/highgui/doc/reading_and_writing_images_and_video
# onder VideoCapture :: get
r = 0
devider = 20

while True:
    while (n<20):
        retval,img = webcam.read()
        print(n)
        if(retval == True):
            img_gray = cv2.cvtColor(img ,cv2.COLOR_BGR2GRAY)
            n+=1
            time.sleep(0.5)
            img_grayold = np.add(img_gray,img_grayold)
            cv2.imwrite("image/gemiddelden%s.png" %n,img_gray)
            
    if (r != 1):
        img_grayav=img_grayold / devider
        img_grayav.astype(np.uint8)
        cv2.imwrite("image/totaal.png", img_grayav)

    
    retval,img = webcam.read()   # inlezen van het beeld in de variabele img
    
    r=1
    if(retval == True ):         # als retval ( return value ) True is , is het

        img_gray = cv2.cvtColor(img ,cv2.COLOR_BGR2GRAY)
        cv2.imshow("beeld2", img)
        cv2.imshow("beeld1",img_gray) # beeld in orde , en kunnen we deze weergeven
        thisimg = cv2.imread('image/totaal.png',-1)
        cv2.imshow("gemiddelde", thisimg)
        
        if(cv2.waitKey(10) == 27): # als Escape key (toetscode 27) wordt
            
            break                 # ingedrukt wordt er uit de while - lus gesprongen
            
webcam.release()
#cv2.destroyWindow("beeld1")
cv2.destroyAllWindows()

