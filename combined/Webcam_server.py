# -*- coding: utf-8 -*-
"""
Created on Wed Aug 30 11:36:39 2017

@author: tretron
"""

#server script

import socket
import struct
import numpy as np
import cv2


webcam = cv2.VideoCapture(0)
# stel aantal pixels over de breedte in
webcam.set(cv2.CAP_PROP_FRAME_WIDTH,640)
# stel aantal pixels over de hoogte in
webcam.set(cv2.CAP_PROP_FRAME_HEIGHT,480) 
webcam.get(cv2.CAP_PROP_FRAME_WIDTH)
cv2.namedWindow("sending image",cv2.WINDOW_AUTOSIZE)

def get_grey():
    img = webcam_img()
    img_grey = cv2.cvtColor(img ,cv2.COLOR_BGR2GRAY)
    return (img_grey)
def webcam_img():
    retval,img = webcam.read()
    if(retval == True):
        return (img)

def destroy_release():    
    webcam.release()
    cv2.destroyAllWindows()

def encode(origonal):
    #encoded = np.zeros((480,640), dtype=np.uint8)
    data_code= "<%df" %len(origonal)
    encoded = struct.pack(data_code,origonal)
    return (encoded)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


host = '127.0.0.1'
#host = 'localhost'
port = 12345
s.bind((host,port))
s.listen(5)

while True:
    
    cv2.imshow("sending image",get_grey())
    print(encode(get_grey()))
    
    if(cv2.waitKey(10) == 27): break

destroy_release()
