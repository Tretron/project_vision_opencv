# -*- coding: utf-8 -*-
"""
Created on Wed Aug 30 11:36:39 2017

@author: tretron
"""

#server script

import socket
import struct
import numpy as np

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

send_array= np.arange(0,100)

host = '127.0.0.1'
#host = 'localhost'
port = 12345
s.bind((host,port))
s.listen(5)

while True:
    c, addr = s.accept()
    print('Got connection from', addr)
    print('sending the following array', send_array)
    byte_code = "<%df" %len(send_array)
    send_array_pack = struct.pack(byte_code, *send_array)
    c.send(send_array_pack)
    result = c.recv(1024)
    recieve_array = struct.unpack(byte_code,result)
    print('recieving the following array', recieve_array)
    
    c.close
